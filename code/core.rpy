init -1000 python:
    # Пути
    config_session = False
    lt_path = "mods/lat/"  # изменить на выходе

    lt_source = lt_path + "source/"

    def lt_images(file):
        return lt_source + "images/%s" % (file)

    def lt_effects(file):
        return lt_source + "images/effects/%s" % (file)

    def lt_gui(file):
        return lt_source + "images/gui/%s" % (file)

init -1 python:
    lt_ambience = {}
    lt_music = {}
    lt_sfx = {}
    for i in renpy.list_files():
        if i.startswith(("lat/source/images/bg/", "lat/source/images/cg/")) and i.endswith((".png", ".jpg")):
            renpy.image((str(i)[21:-4]), i)

        if i.startswith(("lat/source/images/effects/")) and i.endswith((".png", ".jpg")):
            renpy.image((str(i)[26:-4]), i)

        if i.startswith(("lat/source/images/gui/")) and i.endswith((".png", ".jpg")):
            renpy.image((str(i)[22:-4]), i)

        if i.startswith(("lat/source/sound/ambience/")) and i.endswith((".ogg")):
            lt_ambience[i[26:-4]] = i

        if i.startswith(("lat/source/sound/music/")) and i.endswith((".ogg")):
            lt_music[i[23:-4]] = i

        if i.startswith(("lat/source/sound/sfx/")) and i.endswith((".ogg")):
            lt_sfx[i[21:-4]] = i


init 1000:
    $ config.developer = True  # TODO В релиз попасть не должно

init -1 python:
    
    lt_save_version = "0.0.0"

    store.lt_mod_name = "Разврат и Тентакли"
    store.lt_version = "0.0.0"

    lt_version = store.lt_version

    save_name = store.lt_mod_name

    mods["lat_start"] = store.lt_mod_name

# Переменные
init 5:
    call lt_allvars
    $ lt_forgeteveryone()


