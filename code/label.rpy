label lt_allvars:
    init python:
        lt_char = "Выбор" # Рома, Алексей, Артём

    # Дорутовые
    call lt_rm_common_vars
    call lt_ar_common_vars
    call lt_al_common_vars
    return

label lt_rm_common_vars:
    python:
        lt_kr = 0
        lt_st = 0
        lt_an = 0
        lt_mz = 0
    return

label lt_ar_common_vars:
    python:
        lt_us = 0
        lt_ya = 0
        lt_mi = 0
        lt_uv = 0 # Флаги?
    return

label lt_al_common_vars:
    python:
        lt_mz = 0
        lt_dv = 0
        lt_sl = 0
        lt_un = 0
    return


# Запуск мода
label lat_start:
    scene bg black with fade
    $ renpy.block_rollback()
    python:

        if not "Разврат и Тентакли" in config.version: # закидываем себя в трейс на случай армаггедеца игре
            config.version = config.version + ": Разврат и Тентакли ver %s" % (lt_version)

        lt_forgeteveryone()

        persistent.sprite_time = "night"
        lt_prolog_time()
        lt_save_init()

    scene bg black
    call lt_allvars

    if persistent.lt_launched == None:
        # Проверка на запуск, при нём применяются настройки выше и больше не трогаются
        $ persistent.lt_launched = True

    $ renpy.block_rollback()
    call screen lt_charselect
    pause(1)

    if lt_char == "Алексей":
        jump lt_al_route
    elif lt_char == "Рома":
        jump lt_rm_route
    else:
        jump lt_ar_route
    return
