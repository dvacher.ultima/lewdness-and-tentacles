init python:
    lt_std_set_for_preview = {}
    lt_std_set = {}
    store.lt_colors = {}
    store.lt_names = {}
    store.lt_names_list = []
    time_of_day = "night"

    _show_two_window = True

    store.lt_names_list.append("narrator")

    store.lt_names_list.append("lt_th")

    #     #Day - база
    #     #Sunset - 94%, 82%, 100%
    #     #Night - 58%, 67%, 67%
    #     #Prologue - 84%, 72%, 100%
    #     #RGBA
    lt_colors["lt_voice"] = {"night": (225, 221, 125, 255), "sunset": (225, 221, 125, 255), "day": (225, 221, 125, 255), "prolog": (225, 221, 125, 255)}
    lt_names["lt_voice"] = "Голос"
    store.lt_names_list.append("lt_voice")

    lt_colors["lt_myself"] = {"night": (225, 221, 125, 255), "sunset": (225, 221, 125, 255), "day": (225, 221, 125, 255), "prolog": (225, 221, 125, 255)}
    lt_names["lt_myself"] = "Я"
    store.lt_names_list.append("lt_myself")

    lt_colors["lt_el"] = {"night": (205, 205, 0, 255), "sunset": (255, 255, 0, 255), "day": (255, 255, 0, 255), "prolog": (255, 255, 0, 255)}
    lt_names["lt_el"] = "Электроник"
    store.lt_names_list.append("lt_el")

    lt_colors["lt_un"] = {"night": (170, 100, 217, 255), "sunset": (185, 86, 255, 255), "day": (185, 86, 255, 255), "prolog": (185, 86, 255, 255)}
    lt_names["lt_un"] = "Лена"
    store.lt_names_list.append("lt_un")

    lt_colors["lt_dv"] = {"night": (210, 139, 16, 255), "sunset": (255, 170, 0, 255), "day": (255, 170, 0, 255), "prolog": (255, 170, 0, 255)}
    lt_names["lt_dv"] = "Алиса"
    store.lt_names_list.append("lt_dv")

    lt_colors["lt_sl"] = {"night": (214, 176, 0, 255), "sunset": (255, 210, 0, 255), "day": (255, 210, 0, 255), "prolog": (255, 210, 0, 255)}
    lt_names["lt_sl"] = "Славя"
    store.lt_names_list.append("lt_sl")

    lt_colors["lt_us"] = {"night": (234, 55, 0, 255), "sunset": (255, 50, 0, 255), "day": (255, 50, 0, 255), "prolog": (255, 50, 0, 255)}
    lt_names["lt_us"] = "Ульяна"
    store.lt_names_list.append("lt_us")

    lt_colors["lt_mt"] = {"night": (0, 182, 39, 255), "sunset": (0, 234, 50, 255), "day": (0, 234, 50, 255), "prolog": (0, 234, 50, 255)}
    lt_names["lt_mt"] = "Ольга Дмитриевна"
    store.lt_names_list.append("lt_mt")

    lt_colors["lt_cs"] = {"night": (134, 134, 230, 255), "sunset": (165, 165, 255, 255), "day": (165, 165, 255, 255), "prolog": (165, 165, 255, 255)}
    lt_names["lt_cs"] = "Виола"
    store.lt_names_list.append("lt_cs")

    lt_colors["lt_mz"] = {"night": (84, 129, 219, 255), "sunset": (114, 160, 255, 255), "day": (74, 134, 255, 255), "prolog": (74, 134, 255, 255)}
    lt_names["lt_mz"] = "Женя"
    store.lt_names_list.append("lt_mz")

    lt_colors["lt_mi"] = {"night": (0, 180, 207, 255), "sunset": (0, 252, 255, 255), "day": (0, 222, 255, 255), "prolog": (0, 222, 255, 255)}
    lt_names["lt_mi"] = "Мику"
    store.lt_names_list.append("lt_mi")

    lt_colors["lt_uv"] = {"night": (64, 208, 0, 255), "sunset": (78, 255, 0, 255), "day": (78, 255, 0, 255), "prolog": (78, 255, 0, 255)}
    lt_names["lt_uv"] = "Юля"
    store.lt_names_list.append("lt_uv")

    lt_colors["lt_sh"] = {"night": (205, 194, 18, 255), "sunset": (255, 242, 38, 255), "day": (255, 242, 38, 255), "prolog": (255, 242, 38, 255)}
    lt_names["lt_sh"] = "Шурик"
    store.lt_names_list.append("lt_sh")

    lt_colors["lt_pi"] = {"night": (230, 0, 0, 255), "sunset": (230, 0, 0, 255), "day": (230, 1, 1, 255), "prolog": (230, 0, 0, 255)}
    lt_names["lt_pi"] = "Пионер"
    store.lt_names_list.append("lt_pi")

    lt_colors["lt_bush"] = {"night": (192, 192, 192, 255), "sunset": (192, 192, 192, 255), "day": (192, 192, 192, 255), "prolog": (192, 192, 192, 255)}
    lt_names["lt_bush"] = "Голос"
    store.lt_names_list.append("lt_bush")

    lt_colors['lt_chat'] = {'night': (64, 38, 65, 255), 'sunset': (103, 47, 97, 255), 'day': (110, 57, 97, 255), 'prolog': (92, 41, 97, 255)}
    lt_names['lt_chat'] = 'Собеседник'
    store.lt_names_list.append('lt_chat')#Собеседник

    lt_colors['lt_mother'] = {'night': (144, 11, 72, 255), 'sunset': (234, 13, 107, 255), 'day': (249, 16, 107, 255), 'prolog': (209, 12, 107, 255)}
    lt_names['lt_mother'] = "Мама"
    store.lt_names_list.append('lt_mother')#Мама


    

    def lt_char_define(x,is_nvl=False):
        global DynamicCharacter
        global _show_two_window
        global nvl
        global store
        global time_of_day
        gl = globals()
        v = "_voice"
        #if is_nvl:
        #    kind = nvl
        kind = nvl if is_nvl else adv
        ctc = None
        show_two_window = _show_two_window if not is_nvl else False
        who_suffix = ":" if is_nvl else None
        what_color = "#fff" if ((time_of_day == u"night") or (time_of_day == u"prolog")) else "#000"
        if is_nvl:
            if  x == 'narrator':
                gl['narrator'] =     Character(None, kind=nvl, what_style="lt_narrator_%s"%time_of_day, what_color = what_color, ctc=ctc, ctc_position="fixed", drop_shadow = [ (2, 2) ], drop_shadow_color = "#000", what_drop_shadow = None, what_drop_shadow_color = None)
                return
            if  x == 'lt_th':
                gl['lt_th'] =        Character(None, kind=nvl, what_style="lt_thoughts_%s"%time_of_day, what_color = what_color, ctc=ctc, ctc_position="fixed", drop_shadow = [ (2, 2) ], drop_shadow_color = "#000", what_drop_shadow = None, what_drop_shadow_color = None, what_italic = True)
                return
            gl[x] = DynamicCharacter("%s_lt_name"%x, kind=nvl, what_style="lt_normal_%s"%time_of_day, what_color = what_color, ctc=ctc, ctc_position="fixed", drop_shadow = [ (2, 2) ], drop_shadow_color = "#000", what_drop_shadow = None, what_drop_shadow_color = None, color = store.lt_colors[x][time_of_day], who_suffix = ":", show_two_window = show_two_window)
            gl["%s_lt_name"%x] = store.lt_names[x]
        else:
            if  x == 'narrator':
                gl['narrator'] =     Character(None, what_style="lt_narrator_%s"%time_of_day, what_color = what_color, ctc=ctc, ctc_position="fixed", drop_shadow = [ (2, 2) ], drop_shadow_color = "#000", what_drop_shadow = None, what_drop_shadow_color = None)
                return
            if  x == 'lt_th':
                gl['lt_th'] =        Character(None, what_style="lt_thoughts_%s"%time_of_day, what_color = what_color, ctc=ctc, ctc_position="fixed", drop_shadow = [ (2, 2) ], drop_shadow_color = "#000", what_drop_shadow = None, what_drop_shadow_color = None, what_italic = True)
                return
            gl[x] = DynamicCharacter("%s_lt_name"%x, what_style="lt_normal_%s"%time_of_day, what_color = what_color, ctc=ctc, ctc_position="fixed", drop_shadow = [ (2, 2) ], drop_shadow_color = "#000", what_drop_shadow = None, what_drop_shadow_color = None, color = store.lt_colors[x][time_of_day], show_two_window = show_two_window)
            gl["%s_lt_name"%x] = store.lt_names[x]

    def lt_mode_adv():
        nvl_clear()
        
        global menu
        menu = renpy.display_menu
        
        global store
        for x in store.lt_names_list:
            lt_char_define(x)

    def lt_mode_nvl(clear=True):
        if clear:
            nvl_clear()
        
        global menu
        menu = nvl_menu
        
        global narrator
        global lt_th
        narrator_nvl = narrator
        th_nvl = lt_th
        
        global store
        for x in store.lt_names_list:
            lt_char_define(x,True)

    def lt_reload_names():
        global store
        for x in store.lt_names_list:
            lt_char_define(x)

    lt_mode_adv()
    lt_reload_names()

    def lt_meet(who, name):
        global store
        global lt_known_list
        gl = globals()
        gl[who + "_name"] = name
        store.lt_names[who] = name
        lt_reload_names()
        if menu == nvl_menu:
            lt_mode_nvl(False)
        else:
            lt_mode_adv()

    def lt_save_names_known():
        gl = globals()
        global store
        for x in store.lt_names_list:
            if not (x == 'narrator' or x == 'th'):
                store.lt_names[x] = gl["%s_name"%x]


    def lt_forgeteveryone():
        global store
        lt_meet('lt_voice', u"Голос")
        lt_meet('lt_myself', u"Некто")
        lt_meet('lt_mi', u"Азиатка")
        lt_meet('lt_sl', u"Блондинка")
        lt_meet('lt_dv', u"Рыжая")
        lt_meet('lt_us', u"Девочка-СССР")
        lt_meet('lt_un', u"Стесняшка")
        lt_meet('lt_mt', u"Вожатая")
        lt_meet('lt_cs', u"Медсестра")
        lt_meet('lt_dreamgirl', u"...")
        lt_meet('lt_el', u"Блондин")
        lt_meet('lt_pi', u"Пионер")
        lt_meet('lt_sh', u"Очкарик")
        lt_meet('lt_uv', u"Девушка")
        lt_meet('lt_chat', u'Ребёнок')
        lt_meet('lt_mother', u"Мама")
        lt_meet('lt_ami', u"Амина")
        lt_meet('lt_ai', u"Искин")
        lt_meet('lt_os', u'Олег Степанович')
        lt_meet('lt_med', u'Доктор')
        lt_meet('lt_guard', u'Охранник')

    def lt_meeteveryone():
        global store
        lt_meet('lt_voice', u"Голос")
        lt_meet('lt_myself', u"Я")
        lt_meet('lt_mi', u"Мику")
        lt_meet('lt_sl', u"Славя")
        lt_meet('lt_dv', u"Алиса")
        lt_meet('lt_us', u"Ульяна")
        lt_meet('lt_un', u"Лена")
        lt_meet('lt_mt', u"Ольга Дмитриевна")
        lt_meet('lt_cs', u"Виола")
        lt_meet('lt_dreamgirl', u"Харон")
        lt_meet('lt_el', u"Электроник")
        lt_meet('lt_pi', u"Пионер")
        lt_meet('lt_sh', u"Шурик")
        lt_meet('lt_uv', u"Харон")
        lt_meet('lt_chat', u'Друг')
        lt_meet('lt_mother', u"Мама")
        lt_meet('lt_ami', u"Девушка")
        lt_meet('lt_ai', u'Искин')
        lt_meet('lt_os', u'Олег Степанович')
        lt_meet('lt_med', u'Доктор')
        lt_meet('lt_guard', u'Охранник')


    lt_forgeteveryone()
    lt_mode_adv()
    lt_reload_names()

init 5 python:
    def lt_day_time():
        any_time('day')
        persistent.timeofday='day'
        lt_reload_names()
    def lt_sunset_time():
        any_time('sunset')
        persistent.timeofday='sunset'
        lt_reload_names()
    def lt_night_time():
        any_time('night')
        persistent.timeofday='night'
        lt_reload_names()
    def lt_prolog_time():
        any_time('prolog')
        persistent.timeofday='prologue'
        lt_reload_names()
