
screen lt_hover_ro():
    add lt_gui("lt_hover.png") xalign 0.0 yalign 0.0
    
screen lt_hover_al():
    add lt_gui("lt_hover.png") xalign 0.5 yalign 0.0

screen lt_hover_ar():
    add lt_gui("lt_hover.png") xalign 1.0 yalign 0.0

screen lt_charselect:
    tag menu
    modal True
    
    imagemap:
        ground lt_gui("lt_ground.png")
        hotspot ((0, 0, 640, 1080)):
            # Рома
            hovered [Show("lt_hover_ro", transition=Dissolve(0.5))]
            unhovered [Hide("lt_hover_ro", transition=Dissolve(1.0))]
            action [Hide("lt_chatselect", transition=Dissolve(0.5)), SetVariable("lt_char", "Рома"), Return()]
        hotspot ((640, 0, 640, 1080)):
            hovered [Show("lt_hover_al", transition=Dissolve(0.5))]
            unhovered [Hide("lt_hover_al", transition=Dissolve(1.0))]
            action [Hide("lt_chatselect", transition=Dissolve(0.5)), SetVariable("lt_char", "Алексей"), Return()]
        hotspot ((1280, 0, 640, 1080)):
            hovered [Show("lt_hover_ar", transition=Dissolve(0.5))]
            unhovered [Hide("lt_hover_ar", transition=Dissolve(1.0))]
            action [Hide("lt_chatselect", transition=Dissolve(0.5)), SetVariable("lt_char", "Артём"), Return()]
