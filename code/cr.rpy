# Call-Return система
# Рома
label lt_rm_route:
    $ renpy.block_rollback()
    scene bg black
    call lt_rm_common_vars
    pause(1)
    call lt_rm_day_1_start
    pause(1)
    return

label lt_rm_day1_start:
    scene bg ext_road_day with fade
    $ lt_day_time()
    $ persistent.sprite_time = "day"
    "Ы"